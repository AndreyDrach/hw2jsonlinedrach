// Какие существуют типы данных в Javascript?
// null, undefined, boolean, string, number, object)

// В чем разница между == и === ?
// == - равенство, сравнивает значения. === - строгое равенство, сравнивает значения и типы.

// Что такое оператор?
// Знак операции, такой как +,-,/ и пр. называют оператором, большинство операторов соответствуют математическим операциям.

let userName = prompt("Enter your name");
let userAge = prompt("Enter your age");

while (userName === "" || userName === null || userAge === "" || userAge === null || isNaN(userAge)) {
  userName = prompt("Re-enter your name");
  userAge = prompt("Re-enter your age");
}
if (userAge < 18) {
  alert("You are not allowed to visit this website");
}
else if (userAge >= 18 && userAge <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${userName}`);
  } else alert("You are not allowed to visit this website");
} 
else {alert(`Welcome, ${userName}`);
}
